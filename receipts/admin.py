from django.contrib import admin
from receipts.models import Receipts, ExpenseCategory, Account

# Register your models here.


class ReceiptsAdmin(admin.ModelAdmin):
    pass


class ExpenseCategoryAdmin(admin.ModelAdmin):
    pass


class AccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Receipts, ReceiptsAdmin)
admin.site.register(ExpenseCategory, ExpenseCategoryAdmin)
admin.site.register(Account, AccountAdmin)
