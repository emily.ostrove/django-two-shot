from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from receipts.models import Receipts, Account, ExpenseCategory

# from django.contrib.auth.decorators import login_required
# Create your views here.


class ReceiptsListView(ListView):
    model = Receipts
    template_name = "receipts/list.html"


class ReceiptsCreateView(CreateView):
    model = Receipts
    template_name = "receipts/new.html"
    fields = [
        "vendor",
        "total",
        "tax",
        "date",
        "category",
        "account",
    ]
    success_url = reverse_lazy("receipts_list")


class AccountListView(ListView):
    model = Account
    template_name = "accounts/list.html"


class AccountCreateView(CreateView):
    model = Account
    template_name = "accounts/new.html"
    fields = [
        "name",
        "number",
    ]
    success_url = reverse_lazy("accounts_list")


class ExpenseCategoryListView(ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"


class ExpenseCategoryCreateView(CreateView):
    model = ExpenseCategory
    template_name = "categories/new.html"
    fields = ["name"]
    success_url = reverse_lazy("categories_list")


###### moving to accounts.view.py
# def signup(request):
#     if request.method == "POST":
#         form = UserCreationForm(request.POST)
#         if form.is_valid():
#             usernameAPP = request.POST.get("username")
#             passwordAPP = request.POST.get("password1")
#             # User is a model with a function create_user
#             # setting our variables usernameAPP and passwordAPP
#             # to the create_user parameters
#             user = User.objects.create_user(
#                 username=usernameAPP, password=passwordAPP
#             )
#             user.save()
#             login(request, user)
#             return render(request, "home")
#     else:
#         form = UserCreationForm(request.POST)
#     context = {"form": form}
#     return render(request, "registration/sign_up.html", context)


### whatever I got from the internet
# def sign_up(request):
#     context = {}
#     form = UserCreationForm(request.POST or None)
#     if request.method == "POST":
#         if form.is_valid():
#             user = form.save()
#             login(request, user)
#             return render(request, "receipts/list.html")
#     context["form"] = form
#     return render(request, "registration/sign_up.html", context)
