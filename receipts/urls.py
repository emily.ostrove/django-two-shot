from django.contrib import admin
from django.urls import path

# from django.contrib.auth import views as auth_views

from receipts.views import (
    ReceiptsListView,
    ReceiptsCreateView,
    AccountCreateView,
    AccountListView,
    ExpenseCategoryListView,
    ExpenseCategoryCreateView,
)


urlpatterns = [
    # path(
    #     "accounts/login/",
    #     auth_views.LoginView.as_view(),
    #     name="login",
    # ),
    # path(
    #     "accounts/logout/",
    #     auth_views.LogoutView.as_view(),
    #     name="logout",
    # ),
    # path("accounts/signup/", signup, name="sign_up"),
    path("", ReceiptsListView.as_view(), name="home"),
    path("create/", ReceiptsCreateView.as_view(), name="receipts_new"),
    path("accounts/", AccountListView.as_view(), name="accounts_list"),
    path("accounts/create/", AccountCreateView.as_view(), name="accounts_new"),
    path(
        "categories/",
        ExpenseCategoryListView.as_view(),
        name="categories_list",
    ),
    path(
        "categories/create/",
        ExpenseCategoryCreateView.as_view(),
        name="categories_new",
    ),
]
