from unicodedata import category
from django.db import models
from django.conf import settings

# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL


class Receipts(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(
        max_digits=10,
        decimal_places=3,
        null=True,
    )
    tax = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    date = models.DateTimeField(blank=True, null=True)
    purchaser = models.ForeignKey(
        USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
    category = models.ForeignKey(
        "ExpenseCategory",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
    account = models.ForeignKey(
        "Account",
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20, null=True)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.name
